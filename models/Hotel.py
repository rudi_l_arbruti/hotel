import uuid


class Hotel:

    id = uuid.uuid1()

    def __init__(self, pnom, pchambre):
        self.id = uuid.uuid1()
        self.nom = pnom
        self.list_chambre = pchambre


    def serialize(self):
        data = {
            "id": self.id,
            "nom": self.nom,
        }

        return data

