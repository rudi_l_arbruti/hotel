import uuid


class Categorie:

    id = uuid.uuid1()

    def __init__(self, plibelle, pprix):
        self.id = uuid.uuid1()
        self.libelle = plibelle
        self.prix = pprix

    def serialize(self):
        data = {
            "id": self.id,
            "libelle": self.libelle,
            "prix": self.prix,
        }

        return data

