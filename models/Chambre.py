import uuid


class Chambre:

    id = uuid.uuid1()

    def __init__(self, pnumero, pcategorie):
        self.id = uuid.uuid1()
        self.numero = pnumero
        self.categorie = pcategorie

    def serialize(self):
        data = {
            "id": self.id,
            "numero": self.numero,
        }

        return data

