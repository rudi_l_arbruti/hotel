import uuid


class User:

    id = uuid.uuid1()

    def __init__(self,plogin,ppassword, pnom, pprenom, ptelephone, pemail, padresse, pservice):
        self.id = uuid.uuid1()
        self.login = plogin
        self.password = ppassword
        self.nom = pnom
        self.prenom = pprenom
        self.telephone = ptelephone
        self.email = pemail
        self.adresse = padresse
        self.service = pservice

    def serialize(self):
        data = {
            "id": self.id,
            "nom": self.nom,
            "prenom": self.prenom,
        }

        return data

