import uuid


class Sejour:

    id = uuid.uuid1()

    def __init__(self, pclient, pchambre, pdateDebut, pdateFin, puser):
        self.id = uuid.uuid1()
        self.client = pclient
        self.chambre = pchambre
        self.dateDebut = pdateDebut
        self.dateFin = pdateFin
        self.user = puser

    def serialize(self):
        data = {
            "id": self.id,
        }

        return data

