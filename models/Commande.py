import uuid


class Client:

    id = uuid.uuid1()

    def __init__(self, plist_produits, pclient):
        self.id = uuid.uuid1()
        self.list_produits = plist_produits
        self.client = pclient


    def serialize(self):
        data = {
            "id": self.id,
        }

        return data
