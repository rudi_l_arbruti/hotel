import uuid


class Client:

    id = uuid.uuid1()

    def __init__(self, pnom, pprenom, ptelephone, pnum_carte, padresse):
        self.id = uuid.uuid1()
        self.nom = pnom
        self.prenom = pprenom
        self.telephone = ptelephone
        self.num_carte = pnum_carte
        self.adresse = padresse

    def serialize(self):
        data = {
            "id": self.id,
            "nom": self.nom,
            "prenom": self.prenom,
        }

        return data

