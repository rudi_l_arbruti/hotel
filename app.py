from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/user")
def getUser():
    return "User"

@app.route("/user/create")
def addUser():
    return "User"

@app.route("/user/edit/")
def editUser():
    id = request.args.get('id')
    return "User"

@app.route("/user/remove/")
def deleteUser():
    id = request.args.get('id')
    return "User"

@app.route("/client")
def getClient():
    return "Client"

@app.route("/client/create")
def addClient():
    return "Client"

@app.route("/client/edit/")
def editClient():
    id = request.args.get('id')
    return "Client"

@app.route("/client/remove/")
def deleteClient():
    id = request.args.get('id')
    return "Client"

if __name__ == '__main__':
    app.run(debug=True)
